# Paprika to Grocery Markdown

This is a project to convert Paprika 3 recipe format to the Grocery App Markdown recipe format.

This project is replacing my previous blog post: https://www.briankohles.com/blog/2020-06-30-Converting-recipes-paprika-to-markdown/

- [Grocery App Recipe Markdown Format](https://github.com/cnstoll/Grocery-Recipe-Format)

This project contains two scripts

- **paprikaToMd.python** - This script takes in an export of the paprika 3 recipes, and exports them into a Markdown format compatible with the Grocery App
  - This script is ment to be used as a one time conversion from Paprika to Markdown for use in a markdown friendly static site generator blog.
- **copyFromBlogToIcloudGroceryApp.python** - This script will convert the Markdown files from blog friendly format (with YAML frontmatter) to a Grocery App Markdown (no frontmatter), and copy the files to the icloud drive location where Grocery App expects them.
  - This is planned to be run periodically to keep the grocery app at the same data set as the blog directory.

## Running the script

  **Note:** These instructions were written for mac & have not been tested on Linux or Windows.

This is an early copy of this script and it would be helpful to have at least some knowledge of scripting/python before beginning.

These scripts require python3 in order to run. If this isn't installed you will need to get it installed and functional ahead of time.

Also while this script will batch convert your recipes, it is really best to manually edit each recipe afterwords to get it formatted into the format that Grocery App uses, which makes more sense.

A typical recipe breaks the content down into two sections, the ingredients, and the directions, and this is how Paprika stores the data. This is fine and has worked for a very long time, but in my opinion doesn't really make sense. Years ago I started writing my recipes out as a series of ingredients grouped together based on how you used them. The recipe format for Grocery App uses this format as well, and even adds the ability to add timers at any point.

What I'll be doing is reviewing my recipes after converting them & re-organizing them so they follow that format.

If you don't have any programming knowledge my next endevour will be to get this migrated to a web service where you could upload your paprika content and it would be converted for you, but that may take some time.


### Clone/download The Project

The first step is to clone or download the project. If you are unfamiliar with Git/Gitlab using the "Downlaod" link is probably the best choice.

Next extract the downloaded file to a location of your choosing. The project will lay down a few directories, files, and scripts:

```
- **paprika-to-grocery-markdown**
  - **import** -- The directory where you'll save your paprika exports to.
    - **README.markdown** -- A file explaining the directory (and to make git manage it).
  - **output** -- The default location where the markdown recipe files will be exported.
    - **README.markdown** -- A file explaining the directory (and to make git manage it).
  - **scripts** -- The directory of the scripts we run for the process
    - **copyToGroceryApp.python** -- The script that will copy the recipes from the _recipes directory to your icloud drive.
    - **paprikaToMd.python** -- The script that will create the markdown files from the exported data.
  - **README.markdown** -- This readme.
```

### Export your recipes from Paprika 3

For the import to work completely we need to create two separate exports.

- **Paprika Recipe Format** -- This is a JSON format that contains all the recipe data.
- **HTML** -- This is all the recipes in HTML format. We only need this as it exports the recipe image files.

Both export types will be saved to the `import` directory.

#### Export in Paprika Recipe Format

1. In **Paprika Recipe Manager 3** choose `File` and `Export...`
1. From the Export dialog that opens choose the following options:
  1. **Save As** - Name the export file as `myRecipes.paprikarecipes` (use this exact case)
  1. Choose the directory location for the `import` directory of this project folder.
  1. **Categories** - `All Recipes`
  1. **Format** - `Paprika Recipe Format`
  1. **Use Unicode Filenames** - `Yes`
  1. Click `Export` to create the file

#### Export in HTML Format

1. In **Paprika Recipe Manager 3** choose `File` and `Export...`
1. From the Export dialog that opens choose the following options:
    1. **Save As** - Name the export file as `myRecipes` (use this exact case)
    1. Choose the directory location for the `import` directory of this project folder.
    1. **Categories** - `All Recipes`
    1. **Format** - `HTML`
  1. **Use Unicode Filenames** - `Yes`
  1. Click `Export` to create the file

### Run the Paprika to Markdown Conversion

1. Open a command line (terminal.app or iterm2.app)
1. Change directories into the scripts directory
1. Run the paprikaToMd.python script using: `./paprikaToMd.python`
  - This will create the markdown files, these can be used in most static site generator blogs that use markdown.
1. Run the copyToGroceryApp.python script using: `./copyToGroceryApp.python`
  - This will copy the markdown recipe files to your icloud directory for the Grocery app.

### Cleaning up the recipes

Once the recipes have been converted you probably want to manually update each recipe to follow the intended format.

Most recipes are broken down into two parts ingredients and instructions. The format that Grocery App wants is to have the ingredients and instructions intermixed so the relevant instructions and notes follow the list of ingredients used in the instructions.

A good example of this format can be found here: https://github.com/cnstoll/Grocery-Recipe-Format#writing-a-recipe

Additionally further manual cleanup may be needed as the process to convert ingredients into the preferred format (INGREDIENT | MEASURE | NOTES) is not perfect, and may have errors.
